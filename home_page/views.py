from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.models import User


class HomePageView(View):
    def get(self, request, resource):
        """
        Processes other pages.
        """
        if not resource:
            resource = 'index.html'

        resource = 'home_page/' + resource

        admin = User.objects.order_by('id').first()

        return render(request, resource, {'admin': admin})
