from django.urls import path

from .views import HomePageView

urlpatterns = [
    path('', HomePageView.as_view(), {'resource': ''}),
    path('<path:resource>', HomePageView.as_view()),
]
